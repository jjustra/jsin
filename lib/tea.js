/* tea [js gui & tools lib] v2020.03 Jirka Justra */
(()=>{
	function C(k,v,u){!C._d&&(C._d={parent:document.body,_s:'width:100%',E_s:'',L_s:'padding:5px;color:#fff;font-size:16px',B_s:'text-align:center',T_s:'height:300px;min-width:100%;max-width:100%;',G_s:'z-index:9999;padding:5px;margin:2px;position:fixed;top:0px;left:0px;width:30%;background-color:rgba(0,0,0,.5);color:#000;font-family:sans-serif;',log_lvl:4,click_fc:function(f,d,p,e){f&&f(d,p,e)}});if(k===u)return C._d;(v!==u)&&(C._d[k]=v);return C._d[k]}
	function S(e,s,a,i,_){s=s||'';a=a||s.split(';');i=i||0;if(!a[i])return e;_=a[i].split(':');e.style[_[0]]=_[1];return S(e,s,a,i+1)}
	function A(e,p){p=p||C('parent');p.appendChild(e);return e}
	function E(t){var e=document.createElement(t);/*A(e,G._l[G._l.length-1]);*/return S(e,C('_s'))}
	function G(s,e,u){s===u&&(s=G._i);s<0&&(s=G._l.length+s);if(s>0||s===0){G._i=s;return (e==u)&&G._l[s]||G._l[s].childNodes[e]}s=C('G_s')+(s||'');G._l=G._l||[];G._l.push(S(A(e||E('div')),s));G._i=-1;return G._l.length-1}
	function D(e,d,a){if(e>=0||e<0||!e)e=G(e).firstChild;a=a||[];a.push(e.value);if(d&&d.length)e.value=d.shift();e=e.nextSibling;if(e)return D(e,d,a);return a}
	function B(s,f,e){e=e||A(S(E('button'),C('B_s')),G());s&&(e.innerText=s);e.onclick=(e)=>{e=e.originalTarget;C('click_fc')(f,D(e.parentNode.firstChild),e.parentNode,e)};return e}
	function T(s,e){e=e||A(E('textarea'),G());s=C('T_s')+(s||'');return S(e,s)}
	function Tea(s){
		Tea.s=s;
		Tea.i=0;
		Tea.g=[];
		Tea._a=[];
		for(var i=1;i<arguments.length;i++)Tea._a.push(arguments[i]);
		return Tea
	}
	Tea.A=function (i,c) {i=i||Tea.i;c=Tea.s.charAt(i);if(!c||c=='|'||c.toLowerCase()!=c){ var s=Tea.s.substr(Tea.i,i-Tea.i);Tea.i=i+(c=='|');return s };return Tea.A(i+1)};
	Tea.XG=function () {Tea.g.push(G(Tea.A()))};
	Tea.XB=function () {B(Tea.A()||Tea._a.shift(),Tea.A()||Tea._a.shift())};
	Tea.XT=function () {T(Tea.A())};
	Tea.XE=function () {A(S(E('input'),C('E_s')),G())};
	Tea.XL=function () {A(S(E('span') ,C('L_s')),G()).innerText=(Tea.A()||Tea._a.shift())};
	Tea.X=function (c,f) {c=c||Tea.s.charAt(Tea.i++);if(!c)return Tea.g;f=f||Tea['X'+c];f&&f();return Tea.X()};
	Tea.S=S;
	Tea._=A;
	Tea.E=E;
	Tea.G=G;
	Tea.D=D;
	Tea.C=C;
	window['T']=Tea;
})();
function P(){console.log.apply(null,arguments)}
function Q(s){return document.querySelectorAll(s)}
function L(a,f,o, i,r, u){i=i||0;r=r||[];if(i==a.length)return o||r;var _=f&&f(i,a[i],o);if(_!==u)r.push(_);return L(a,f,o, i+1,r);}
var LQ=(s,f,o)=>{return L(Q(s),f,o)},TS=T.S,TA=T._,TE=T.E,TG=T.G,TD=T.D,TC=T.C,TX=function(){return T.apply(0,arguments).X()};/* shortcuts */
function __log_proc(s,a,i,r) {i=i||0;r=r||[a.shift()];var rI,bA,bR;rI=r.length-1;bR=(typeof r[rI])[0]=='s';bA=(typeof a[i])[0]=='s';if(s&&bR&&!bA){r[rI]='%c'+r[rI];r.push(s);s=''}if(i==a.length)return r;if(bR&&bA)r[rI]+=' : '+a[i];else r.push(a[i]);return __log_proc(s,a,i+1,r)}
function E(){if(TC('log_lvl')>0){var a=Array.apply(0,arguments);a.unshift('#err',new Date().toISOString(),(new Error).stack.split('\n')[1]);console.log.apply(0,__log_proc('color:#f55',a))}}
function W(){if(TC('log_lvl')>1){var a=Array.apply(0,arguments);a.unshift('#wrn',new Date().toISOString(),(new Error).stack.split('\n')[1]);console.log.apply(0,__log_proc('color:#f5f',a))}}
function I(){if(TC('log_lvl')>2){var a=Array.apply(0,arguments);a.unshift('#inf',new Date().toISOString(),(new Error).stack.split('\n')[1]);console.log.apply(0,__log_proc('color:#55f',a))}}
function D(){if(TC('log_lvl')>3){var a=Array.apply(0,arguments);a.unshift('#dbg',new Date().toISOString(),(new Error).stack.split('\n')[1]);console.log.apply(0,__log_proc('color:#444',a))}}