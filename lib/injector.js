// requires tea.js

var _injector=[]

function injector (e) {
	var url=e.url
	
	if (url.indexOf('about:')==0) return
	if (url.indexOf('moz-extension://')==0) return
	
	L(_injector,function (i,o) {
		var err,use_this=1,only_neg_oneliners=0;// no rules|only non-mathcing neg-oneliners => inject everywhere
		var rule_a,rule_a_l,rule_a_i
		var word_a,word_a_l,word_a_i,word_t
		
		if (o.sfilter.length) {// loop rules to find out if we inject
			use_this=undefined
			only_neg_oneliners=1
			rule_a=o.sfilter
			rule_a_l=rule_a.length
			
			for (rule_a_i=0;rule_a_i<rule_a_l;rule_a_i++) {
				word_a=rule_a[rule_a_i]
				word_a_l=word_a.length
				err=0
				
				for (word_a_i=0;word_a_i<word_a_l;word_a_i++) {
					word_t=word_a[word_a_i]
					if ( (url.indexOf(word_t[0])>=0)!=word_t[1]) { err=1;break }
				}
				
				// if only one negation rule is in line, it negates whole filter on failure
				if (word_a_l==1 && !word_a[0][1]) {
					if ( err) { use_this=0;break }
				} else {
					only_neg_oneliners=0
					if (!err) { use_this=1;break }
				}
			}
			
			if (use_this===undefined) {
				if (only_neg_oneliners)
					use_this=1
				else
					use_this=0
			}
		}
		
		if (use_this) {
			D('injector_inject',e.url,'is using',o.name)
			if (o.js) {
				browser.tabs.executeScript(e.tabId,{code:o.js,allFrames:true});
			} else W('nothing to inject from',o.name)
		}
	})
}


function injector_load(data){
	_injector=[]
	var use_inject,bool
	L(data,(i,o)=>{
		use_inject=1
		
		o.name=o.name||''
		o.sfilter=o.sfilter||''
		o.js=o.js||''
		
		o.sfilter=L(o.sfilter.split('\n'),(i,w)=>{
			s=w.trim()
			if(!s||s.charAt(0)=='#')return
			if (s=='!') {
				D('[disabled]',o.name)
				use_inject=0
				return
			}
			s=s.replace(/\t/g,' ')
			s=L(s.split(' '),(i,s)=>{
				if(!s.length)return
				bool=true
				if (s.charAt(0)=='!') {
					bool=false
					s=s.slice(1)
				}
				return [s,bool]
			})
			return s
		})
		
		if (use_inject) _injector.push(o)
	})
	D('_injector loaded',_injector)
}

