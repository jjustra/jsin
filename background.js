// requires tea.js
// requires downloader.js
// requires injector.js

TC('log_lvl',3);// no debug

var stor=browser.storage.local
var k='injects'
stor.get([k]).then(
	(o)=>{
		if(o[k])injector_load(o[k])
		I('loaded',_injector)
	},
	()=>{ P('load error') },
)

browser.storage.onChanged.addListener((o,area) => {
	D(area,!!o[k],o);
	if(area=='local' && o[k])
		injector_load(o[k].newValue)
});
browser.webNavigation.onCompleted.addListener(injector);
browser.browserAction.onClicked.addListener((tab) => {
	browser.runtime.openOptionsPage()
});
browser.runtime.onMessage.addListener((o,src)=>{
	if (src.id!='jsin@jirka-justra.cz') return
	D('bg received','src :',src,'msg :',o)
	if (o.download) {// download url/raw-data
		download(o.download,o.path,o.raw,(state)=>{
			browser.tabs.sendMessage(src.tab.id,{id:o.id,result:state})
		})
	}
	if (o.close) {// close sending tab
		browser.tabs.remove(src.tab.id)
	}
	if (o.app) {// send native message
		browser.runtime.sendNativeMessage(o.app,o.data).then((resp)=>{
			browser.tabs.sendMessage(src.tab.id,{id:o.id,result:resp})
		},(e)=>{
			E('native message error','src :',src,'msg :',o,'err :',e)
		});
	}
});

/*
downloads.onCreated.addListener((itm)=>{

	var ext, ext_list;

	ext_list = state.interceptExtensions.split(' ');

	// has no extension
	if (itm.filename.index('.') < 0) return;

	ext = itm.filename.split('.').pop().toLowerCase();

	// extension not in the list
	if (ext_list.indexOf(ext) < 0) return;

	// stop download and add NAS task
	downloads.cancel(itm.id);
//	addDownloadTasksAndPoll

});
*/